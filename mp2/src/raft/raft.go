package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import "sync"
import "sync/atomic"
import "../labrpc"
import "math/rand"
import "time"
import "fmt"
import "strconv"
//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

// Log entry
type Entry struct{
	Command interface{}
	Term	int
}

// A Go object implementing a single Raft peer.
type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	me        int                 // this peer's index into peers[]
	dead      int32               // set by Kill()

	// Your data here (2A, 2B).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.
	// You may also need to add other state, as per your implementation.

	state string 			// FOLLOWER - CANDIDATE - LEADER
	startTime time.Time			// Start time for timeout
	heartbeatTime time.Time		// timefor leader heartbeat
	currentTerm int			// Latest term seen by this process 
	votedFor	ballot // Id of candidate voted for, null if not voted
	log[]		Entry		// List of received log entries
	commitIndex	int 		// Index of highest known committed log entry
	lastApplied int 		// Index of highest entry applied to state machine

	applyCh chan ApplyMsg 	 // Channel to tell leader append entry returned

	commitReadyChan chan struct{} // Channel to tell if a message is ready to be committed

	// For Leaders Only - Metadata on Followers
	nextIndex[]  int 		// Index of next log entry to send to server <i>
	matchIndex[] int 		// Index of highest log entry known to be replicated on server <i>
}

type ballot struct {
	term int
	candidate int
}
// Example RequestVote RPC arguments structure.
// Field names must start with capital letters!
type RequestVoteArgs struct {
	// Your data here (2A, 2B).
	Term int				// Candidate's (Requestor) term 
	CandidateID int // Id of requestor/candidate
	LastLogIndex int		// Index of requestor/candidate last log entry
	LastLogTerm  int		// Term of requestor/candidate last log entry
}

type AppendEntriesArgs struct {
	// Your data here (2A, 2B).
	Term int 			  // Leader's term when sending RPC
	LeaderID int  // Leaders ID
	PrevLogIndex int 	  // Index of log entry right before new ones being sent out
	PrevLogTerm int 	  // Term of log entry right before
	Entries[] 	Entry	  // Entries to append (can be multiple for efficiency)
	LeaderCommit int 	  // Leader's Commit index 
}

// example RequestVote RPC reply structure.
// field names must start with capital letters!
type RequestVoteReply struct {
	// Your data here (2A).
	Term int		 // Term to update to
	VoteGranted bool // True if vote received
	Responded bool // True if this peer has responed
}

type AppendEntriesReply struct {
	// Your data here (2A).
	Term int // Client's current term if leader needs to update itself
	Success bool // True if follower contained entries that matched PrevLogIndex and PrevLogTerm
}



// Example RequestVote RPC handler.
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (2A, 2B).
	// Read the fields in "args", 
	// and accordingly assign the values for fields in "reply".

	// Refresh timeout
	rf.mu.Lock()
	
	// fmt.Printf("Vote from %d to %d \n", args.CandidateID, rf.me)
	// fmt.Printf("voted for term %d vote term %d\n", rf.votedFor.term, args.Term)
	// rf.printLog()
	logTerm := 0
	if len(rf.log) !=0 {
		logTerm = rf.log[len(rf.log) - 1].Term
	}
	if rf.currentTerm < args.Term{
		rf.state = "FOLLOWER"
		rf.votedFor.candidate = -1
		rf.currentTerm = args.Term
	}
	if rf.currentTerm > args.Term{
		
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
		reply.Responded = true 
	} else if rf.currentTerm == args.Term && (rf.votedFor.candidate == -1 || rf.votedFor.candidate == args.CandidateID) && (args.LastLogTerm > logTerm || (args.LastLogTerm == logTerm && args.LastLogIndex >= len(rf.log)-1)){
		rf.startTime = time.Now()
		
		reply.Term = rf.currentTerm
		reply.VoteGranted = true
		reply.Responded = true
		rf.votedFor.candidate = args.CandidateID
		rf.votedFor.term = args.Term
		rf.state = "FOLLOWER"
		// print("here")
		// fmt.Print("id: " +  strconv.Itoa(rf.me) + " term: " + strconv.Itoa(rf.currentTerm))
		// fmt.Print(" ")
		// fmt.Print(rf.state)
		// fmt.Print(" Vote Term: " + strconv.Itoa(rf.votedFor.term) + " Voted For: " +  strconv.Itoa(rf.votedFor.candidate) + " Bool val " + strconv.FormatBool(reply.VoteGranted))
		// fmt.Print("\n")
		
	} else {
		
		reply.Term = rf.currentTerm
		reply.VoteGranted = false
		reply.Responded = true 
	}
	rf.mu.Unlock()
	return
}

// Append Entry RPC Handler
func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	// Refresh timeout
	// print("append entries")
	rf.mu.Lock()
	// print("grabed lock")
	
	
	// Check if valid
	if rf.state == "CANDIDATE" && (args.Term >= rf.currentTerm || args.LeaderCommit > rf.commitIndex) {
		rf.state = "FOLLOWER"
		rf.currentTerm = args.Term
	}
	if rf.state == "LEADER" && args.Term > rf.currentTerm{
		rf.state = "FOLLOWER"
	}
	if rf.currentTerm > args.Term && rf.commitIndex >= args.LeaderCommit {
		//this might be wrong && might be wrong

		reply.Success = false
		reply.Term = rf.currentTerm
		// print("fail")
		rf.mu.Unlock()
		return
	} else {
		rf.currentTerm = args.Term
	}
	if rf.currentTerm < args.Term {
		rf.currentTerm = args.Term
		rf.votedFor.term = args.Term + 1
		rf.votedFor.candidate = -1
	}
	
	// rf.commitIndex = args.LeaderCommit
	// Check heartbeat
	
	rf.startTime = time.Now()

	if args.PrevLogIndex == -1 || (args.PrevLogIndex < len(rf.log) && args.PrevLogTerm == rf.log[args.PrevLogIndex].Term){
		reply.Success = true

		insertIndex := args.PrevLogIndex + 1
		newEntriesIndex := 0
		//find first index in log that needs to be overwritten
		for {
			if insertIndex >= len(rf.log) || newEntriesIndex >= len(args.Entries) {
				break
			}
			if rf.log[insertIndex].Term != args.Entries[newEntriesIndex].Term {
				break
			}
			insertIndex++
			newEntriesIndex++
		}
		if newEntriesIndex < len(args.Entries) {
			//append all new entries to log
			rf.log = append(rf.log[:insertIndex], args.Entries[newEntriesIndex:]...)
		}

		if args.LeaderCommit > rf.commitIndex {
			if args.LeaderCommit < len(rf.log) - 1{
				rf.commitIndex = args.LeaderCommit
			} else {
				rf.commitIndex = len(rf.log) - 1
			}
			// print("commit")
			rf.commitReadyChan <- struct{}{}
			//add in function that makes apply channel calls
			// go rf.commit_entries()

			
		}
	} else {
		//add else statment later to handle optimization
		reply.Success = false
	}
	
	reply.Term = rf.currentTerm
	rf.mu.Unlock()
	return
}



func (rf *Raft) printLog() {
	print( "Id: " + strconv.Itoa(rf.me) +" Log: ")
	for i := 0; i < len(rf.log); i ++{
		fmt.Printf(" Term: %v Cmd: %v Idx: %v ", rf.log[i].Term,rf.log[i].Command , i )
	}
	
	print("Commit Index: " + strconv.Itoa(rf.commitIndex) + "\n")
}



func (rf *Raft) handleCommitChannel(){
	for range rf.commitReadyChan {
		rf.mu.Lock()
		lastApplied := rf.lastApplied
		var entries []Entry
		if rf.commitIndex > rf.lastApplied {
				entries = rf.log[rf.lastApplied + 1: rf.commitIndex + 1]
				rf.lastApplied = rf.commitIndex
		}
	
		for i := 0; i < len(entries); i ++{
			rf.applyCh <- ApplyMsg{
				CommandValid: true,
				Command: entries[i].Command,
				CommandIndex: lastApplied + 2 + i,
			}
		}
		rf.mu.Unlock()
		// rf.printLog()
		
	}
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//

// Request Vote RPC
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	//print("here")
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	//print("here")
	return ok
}

// Send Append Entry RPC
func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)
	
	// Tell leader which client replied
	// if rf.state == "LEADER" {
	// 	rf.success <- server 
	// }

	return ok
} 


// Follower Protocol
func (rf *Raft) Follower() {
	// Start listening 
	timeout := time.Duration((300 + (rand.Int() % 200))) * time.Millisecond
	
	rf.mu.Lock()
	rf.state = "FOLLOWER"
	rf.startTime = time.Now()
	rf.mu.Unlock()
	for {
		// Handle timeout or RPC
		
		time.Sleep(10 * time.Millisecond)
		rf.mu.Lock()
		if rf.state != "FOLLOWER"{
			rf.mu.Unlock()
			return
		}
		if rf.killed() {
			rf.mu.Unlock()
			return
		}
		if time.Now().Sub(rf.startTime) >= timeout {
			// print("Timed out Server:" + strconv.Itoa(rf.me) + "\n")
			go rf.Candidate()
			rf.mu.Unlock()
			return
		}
		rf.mu.Unlock()
		
	}
}

func (rf *Raft) Candidate() {
	timeout := time.Duration((400 + (rand.Int() % 300))) * time.Millisecond
	rf.mu.Lock()
	rf.currentTerm += 1
	rf.state = "CANDIDATE"
	// fmt.Printf("candidate: %d \n", rf.me)
	majority := len(rf.peers)/2
	//set up vote args
	
	var args RequestVoteArgs
	args.Term = rf.currentTerm
	args.CandidateID = rf.me
	args.LastLogIndex = len(rf.log) - 1
	if len(rf.log) == 0 {
		args.LastLogTerm = 0
	} else {
		args.LastLogTerm = rf.log[len(rf.log) - 1].Term
	}

	//set up requestvotereply
	var resp []RequestVoteReply
	//start threads for each peer to call request vote
	var voteReply RequestVoteReply
	// for i := 0; i < len(rf.peers); i ++ {
	// 	resp = append(resp, voteReply)
	// 	if rf.me != i {
	// 		go rf.sendRequestVote(i, &args, &resp[i])
	// 	}
	// }
	count := 1
	finished := 1
	timedout := false
	isfollower := false
	var mu sync.Mutex
	cond := sync.NewCond(&mu) 
	for i := 0; i < len(rf.peers); i++ {
		resp = append(resp, voteReply)
		if rf.me != i {
			go func(x int) {
				ok := rf.sendRequestVote(x, &args, &resp[x])
				ok = ok
				mu.Lock()
				defer mu.Unlock()
				// fmt.Print("vote is: " + strconv.FormatBool(resp[x].VoteGranted) + " \n")
				if resp[x].VoteGranted {
					count++
					// fmt.Print("Count: " + strconv.Itoa(count) +" \n")
				}
				finished++
				cond.Broadcast()
			}(i)
		}
	}
	//waits until the timeout value is hit then broadcasts
	go func() {
		time.Sleep(timeout)
		mu.Lock()
		timedout = true
		defer mu.Unlock()
		cond.Broadcast()
		
	} ()
	go func() {
		for {
			time.Sleep(20 * time.Millisecond)
			mu.Lock()
			rf.mu.Lock()
			if rf.state != "CANDIDATE"{
				isfollower = true;
				defer mu.Unlock()
				rf.mu.Unlock()
				cond.Broadcast()
				return
			} else {
				mu.Unlock()
				rf.mu.Unlock()
			}
		}
		
	} ()

	resp[rf.me].Term = rf.currentTerm - 1 
	resp[rf.me].VoteGranted = true

	rf.votedFor.term = rf.currentTerm
	rf.votedFor.candidate = rf.me
	rf.mu.Unlock()

	//isFollower might end up breaking other code could cause timing issues
	mu.Lock()
	for !(count > majority) && (finished != len(rf.peers)) &&  !timedout && !isfollower {
		
		cond.Wait()
		// fmt.Print("Countfdasfasdfa: " + strconv.Itoa(count) +" \n")
	}
	// fmt.Print("Case Hit \n" + "Count: " + strconv.Itoa(count) + "finished " + strconv.Itoa(finished) + " \n")
	rf.mu.Lock()
	
	if rf.killed() {
		mu.Unlock()
		rf.mu.Unlock()
		return
	}
	if rf.state == "FOLLOWER" {
		go rf.Follower()
		mu.Unlock()
		rf.mu.Unlock()
		return

	} else if isfollower || (finished == len(rf.peers) && count <= majority) {
		rf.state = "FOLLOWER"
		go rf.Follower()
		mu.Unlock()
		rf.mu.Unlock()
		return
	}
	if count > majority {
		numLogs := len(rf.log)
		var match[] int
		var next[] int
		for i := 0; i < len(rf.peers); i++ {
			next = append(next, numLogs)
			match = append(match, -1)
		}
		rf.nextIndex = next
		rf.matchIndex = match
		rf.state = "LEADER"
		go rf.Leader()
		mu.Unlock()
		rf.mu.Unlock()
		return
	}
	if timedout {
		go rf.Candidate()
		mu.Unlock()
		rf.mu.Unlock()
		return
	}
	mu.Unlock()
	rf.mu.Unlock()
	return
}


func (rf *Raft) Leader() {
	
	// fmt.Print(strconv.Itoa(rf.me) + "is new leader \n")
	heartbeat := time.Duration(100) * time.Millisecond
	//send heartbeat to take tell others you are leader
	rf.mu.Lock()
	
	rf.heartbeatTime = time.Now()
	rf.mu.Unlock()
	rf.sendHeartbeat()
	var mu sync.Mutex
	cond := sync.NewCond(&mu)
	follower := false
	killed := false
	go func() {
		for {
			time.Sleep(20 * time.Millisecond)
			rf.mu.Lock()
			if rf.state != "LEADER"{
				mu.Lock()
				defer mu.Unlock()
				follower = true
				cond.Broadcast()
				rf.mu.Unlock()
				return
			} else if rf.killed() {
				mu.Lock()
				killed = true
				defer mu.Unlock()
				cond.Broadcast()
				rf.mu.Unlock()
				return
			}
			rf.mu.Unlock()
			
		}
		
	} ()
	go func() {
		for {
			
			time.Sleep(heartbeat)
			if killed || follower{
				return
			}
			rf.sendHeartbeat()
			
			
		}
		
	} ()
	
	
	
	mu.Lock()
	cond.Wait()
	
	if follower {
		mu.Unlock()
		go rf.Follower()
		return
	} else{
		mu.Unlock()
		return
	}
		
}

func (rf *Raft) sendHeartbeat(){
	rf.mu.Lock()
	savedCurrentTerm := rf.currentTerm
	rf.heartbeatTime = time.Now()
	rf.mu.Unlock()
	for i := 0; i < len(rf.peers); i ++ {
		if rf.me != i {
			go func(id int){
				//grab lock to create arguments for AE call
				majority := len(rf.peers)/2
				rf.mu.Lock()
				nextI := rf.nextIndex[id]
				prevLogIndex := nextI - 1
				prevLogTerm := -1
				if prevLogIndex >= 0{
					prevLogTerm = rf.log[prevLogIndex].Term
				}
				outgoing := rf.log[nextI:]
				cmdArgs := AppendEntriesArgs{
					Term: rf.currentTerm, 
					LeaderID: rf.me,
					PrevLogIndex: prevLogIndex,
					PrevLogTerm: prevLogTerm,
					Entries: outgoing,
					LeaderCommit: rf.commitIndex,
				}
				rf.mu.Unlock()
				var resp AppendEntriesReply
				ok := rf.sendAppendEntries(id, &cmdArgs, &resp)
				if ok{
					rf.mu.Lock()
					defer rf.mu.Unlock()
					//check if leader is behind
					if resp.Term > savedCurrentTerm{
						rf.state = "FOLLOWER"
						return
					} else if rf.killed() || rf.state != "LEADER"{
						return
					}
					//check if AE was a success
					if savedCurrentTerm == resp.Term {
						if resp.Success{
							
							//set new match and next index for server
							rf.nextIndex[id] = nextI + len(outgoing)
							rf.matchIndex[id] = nextI + len(outgoing) - 1
							commitIndex_ := rf.commitIndex

							//check if any messages are now ready to be committed

							for i := rf.commitIndex + 1; i < len(rf.log); i ++{
								//only check current term messages since 
								//previous term could have been other leader
								if rf.log[i].Term == rf.currentTerm{
									count := 1
									for j := 0; j < len(rf.peers); j++{
										if j != rf.me{
											if rf.matchIndex[j] >= i{
												count++
											}
										}
										if count > majority{
											rf.commitIndex = i
										} 
									}
								}
							}
							if rf.commitIndex != commitIndex_{
								// print("commiting up to index" + strconv.Itoa(rf.commitIndex))
								rf.commitReadyChan <- struct{}{}
							}
						} else {
							if rf.nextIndex[id] > 0{
								rf.nextIndex[id] = rf.nextIndex[id] -1
							}
							 
						}
					}

					
				}


			}(i)
		}
	}

}

// return currentTerm and whether this server believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	
	term = rf.currentTerm
	isleader = (rf.state == "LEADER")
	// fmt.Print("id: " +  strconv.Itoa(rf.me) + " term: " + strconv.Itoa(rf.currentTerm))
	// fmt.Print(" ")
	// fmt.Print(rf.state)
	// fmt.Print(" Vote Term: " + strconv.Itoa(rf.votedFor.term) + " Voted For: " +  strconv.Itoa(rf.votedFor.candidate))
	// fmt.Print("\n")
	return term, isleader
}

// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	rf.mu.Lock()
	index := -1
	term := -1
	isLeader := (rf.state == "LEADER")

	// Append to leader
	if isLeader {
		nextEntry := Entry{Command: command, Term: rf.currentTerm}
		rf.log = append(rf.log, nextEntry)
		index = len(rf.log)
		term = rf.currentTerm
		
		go rf.sendHeartbeat()
	}

	// Your code here (2B).
	rf.mu.Unlock()
	return index, term, isLeader
}




	// Send channel as argument to RPC caller? Cant be apart of raft struct
	// Keep track of clients that finished - Map?

// }


//
// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
//
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.

}

func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	// if z == 1 {
	// 	fmt.Print("killed")
	// }
	return z == 1
}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.me = me
	
	// Your initialization code here (2A, 2B).
	rf.applyCh = applyCh
	rf.commitReadyChan = make(chan struct{}, 16)
	rf.votedFor.term = -1
	rf.votedFor.candidate = -1
	rf.lastApplied = -1
	rf.commitIndex = -1
	// Instantiate node as a follower
	go rf.handleCommitChannel()
	go rf.Follower()

	return rf
}
