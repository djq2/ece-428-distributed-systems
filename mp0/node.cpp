#include <sys/socket.h>
#include <sys/types.h>
#include <iostream>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <string>
#include <chrono>
#include <fstream>
#include <poll.h>

int split(std::string data, std::pair<std::string, std::string>& out){
    int idx = data.find(' ');
    out.first = data.substr(0, idx);
    out.second = data.substr(idx + 1);
    return idx;
}

int main(int argc, char* argv[]){
    if(argc != 4){
        std::cerr << "Invalid arguments\n";
        return 0;
    }

    int node_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(node_fd == 1){
        std::cerr << "Failed to create socket.\n";
        return 0;
    }

    struct sockaddr_in node_addr;
    bzero((char*)&node_addr, sizeof(sockaddr_in));

    std::string node_name = std::string(argv[1]);
    int name_length = node_name.size();
    struct hostent* server = gethostbyname(argv[2]);
    short port = atoi(argv[3]);

    if(!server){
        std::cerr << "No server w given IP\n";
        return 0;
    }

    node_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&node_addr.sin_addr.s_addr, server->h_length);
    node_addr.sin_port = htons(port);

    if( (connect(node_fd, (sockaddr*) &node_addr, sizeof(node_addr))) == -1 ){
        std::cerr << "Failed to connect to server\n";
        return 0;
    }
    // if( write(node_fd, buf, node_name.size()) == -1)

    // int pipes[2];
    // if(pipe(pipes) == -1){
    //     std::cerr << "Pipe failure\n";
    //     return 0;
    // } 

    std::string input;

    bool with_name = true;

    // struct pollfd pread;
    // pread.fd = fileno(stdin);
    // pread.events = POLLIN;

    while(true){

        char buf[400];
        bzero(buf, 400);
        input.clear();

        // volatile int pval = 1;
        // while( (pval = ) ){std::cout << "empty\n";}

        //std::cin >> tim;
        //std::cerr << tim << "\n";
        // std::cout << "WHERE\n";
        // while((std::cin.peek() == -1 || std::cin.peek() == '\n')) {}

        std::getline(std::cin, input);

        std::pair<std::string, std::string> vals; 
        int div = split(input, vals);

        if(with_name){
            memccpy(buf, vals.first.c_str(), '\0', vals.first.size());
            buf[div] = ' ';
            buf[div + 1] = '-';
            buf[div + 2] = ' ';
            memccpy(buf + div + 3, node_name.c_str(), '\0', name_length);
            buf[div + 3 + name_length] = ' ';
            std::string mes = "connected\n";
            memccpy(buf + div + name_length + 4, mes.c_str(), '\0', 9);
            with_name = false;
        }
        else{
            memccpy(buf, vals.first.c_str(), '\0', vals.first.size());
            buf[div] = ' ';
            memccpy((uint8_t*)buf + (div + 1), node_name.c_str(), '\0', name_length);
            buf[div + 1 + name_length] = ' ';
            
            memccpy((uint8_t*)(buf + div + name_length + 2), vals.second.c_str(), '\0', vals.second.size());     
        }

        if( (write(node_fd, buf, 400)) == -1 ){
            std::cerr << "Failed to write to socket\n";
            return 0;
        }


        // std::cin.ignore();
        // std::getline(std::cin, input);

        // fseek (stdin, 0, SEEK_END);
        // long in_len = ftell (stdin);
        // while(!in_len){
        //     std::cout << "Getting data?\n";
        //     in_len = ftell ();
        // }

        // do{
        //     std::cin >> buf[buf_idx];
        //     test_file << "Writing ?\n";
        //test_file << input << "\n";
        //     buf_idx++;
        // }while( buf[buf_idx -1] != '\n');
  //parse args read in from stdin

        // std::getline(std::cin, input);
        // std::cerr << "Piped data = " << input << "\n";
    }

}